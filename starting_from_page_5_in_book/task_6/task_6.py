class Mathematical_operations_with_right_triangle:

    def __init__(self, x, y):
        self.x = x
        self.y = y

    @classmethod
    def input_arguments(cls):
        return cls (
        int(raw_input('enter parametrs of first Catet: ')),
        int(raw_input('enter parametrs of second Catet: ')),
        )
    def find_the_hypotenuse_of_a_right_triangle(self):
        return (self.x**2+self.y**2)
    def find_the_area_of_a_right_triangle(self):
        return (self.x**2+self.y**2)/2

result_of_mathematical_operations = Mathematical_operations_with_right_triangle.input_arguments()
print('Hypotenuse of a right triangle is......:',result_of_mathematical_operations.find_the_hypotenuse_of_a_right_triangle())
print('Area of a right triangle is......:',result_of_mathematical_operations.find_the_area_of_a_right_triangle())
