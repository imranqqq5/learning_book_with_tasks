class Calculator:
    """
    Example how we can use this math operations in real life
    """
    def __init__(self, first_arg, second_arg):
        self.first_arg = first_arg
        self.second_arg = second_arg

    def handle(self):
        a = input ('Select the operation that we would like to perform 1 (addition), 2 (subtraction), 3 (multiplication).....:')
        if a == 1:
            return self.first_arg + self.second_arg
        if a == 2:
            return self.first_arg - self.second_arg
        if a == 3:
            return self.first_arg * self.second_arg
        else:
            return ('NOT CORRECTLY CHOISE, LET TRY AGAIN?')

first_arg = int(raw_input('first_arg: '))
second_arg = int(raw_input('second_arg: '))

calculator = Calculator(first_arg, second_arg)

print('The result of this task is equal to......:',calculator.handle())
