import math

class Mathematical_Operations:

    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z

    @classmethod
    def input_arguments(cls):
        return cls (
        int(raw_input('x: ')),
        int(raw_input('y: ')),
        int(raw_input('z: ')),
        )
    def arithmetical_mean(self):
        return (self.x+self.y+self.z) / 2
    def geometric_mean(self):
        return math.sqrt(self.x+self.y+self.z)
result_of_mathematical_operations = Mathematical_Operations.input_arguments()
print('The result of this task is equal to......:',result_of_mathematical_operations.arithmetical_mean())
print('The result of this task is equal to......:',result_of_mathematical_operations.geometric_mean())
